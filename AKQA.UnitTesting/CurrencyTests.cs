﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AKQA.Library;

namespace AKQA.UnitTesting
{
    [TestClass]
    public class CurrencyTests
    {
        [TestMethod]
        public void Test_Invalid_Input()
        {
            string input = "2323fgfgfg";
            string strExpectedOutput = "INPUT STRING WAS NOT IN A CORRECT FORMAT.";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Simple_Value()
        {
            string input = "25";
            string strExpectedOutput = "TWENTY FIVE";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Input_With_Cents()
        {
            string input = "25.54";
            string strExpectedOutput = "TWENTY FIVE AND FIFTY FOUR CENTS";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Negative_Value()
        {
            string input = "-150";
            string strExpectedOutput = "MINUS ONE HUNDRED FIFTY";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Hundreds_Value()
        {
            string input = "523";
            string strExpectedOutput = "FIVE HUNDRED TWENTY THREE";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Thousands_Value()
        {
            string input = "3425";
            string strExpectedOutput = "THREE THOUSAND FOUR HUNDRED TWENTY FIVE";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }

        [TestMethod]
        public void Test_Thousands_With_Cents()
        {
            string input = "4567.35";
            string strExpectedOutput = "FOUR THOUSAND FIVE HUNDRED SIXTY SEVEN AND THIRTY FIVE CENTS";
            ICurrencyHandler iCurrency = new CurrencyHandler();
            Assert.AreEqual(strExpectedOutput, iCurrency.ConvertNumberToCurrencyWords(input));
        }
    }
}
