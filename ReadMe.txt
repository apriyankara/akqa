
Applicant: Amila Priyankara (pd.amila@gmail.com)

==The solution has three projects.

1. AKQA.Library
2. AKQA.UnitTesting  (Microsoft.VisualStudio.TestTools.UnitTesting)
3. AKQA.Web (MVC, WebAPI, Angular)

Please note that the WebService has built with WebAPI 2.0

WebAPI-Controller: CurrencyAPIController

JavaScript Client: ServiceConsumer.js

Test View: Home/Index

