﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AKQA.Web.Models;
using AKQA.Library;

namespace AKQA.Web.Controllers
{
    public class CurrencyAPIController : ApiController
    {
        ICurrencyHandler iCurrency = new CurrencyHandler();

        [HttpPost]
        public TestOutput GetCurrencyWords(TestInput input)
        {
            TestOutput result = new TestOutput();
            result.Name = input.Name;
            result.CurrencyWords = iCurrency.ConvertNumberToCurrencyWords(input.Number);

            return result;
        }
    }
}
