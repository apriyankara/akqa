﻿var app = angular.module('homeModule', []);

app.controller('homeControl', function ($scope, $http) {  
 
    $scope.Submit= function ()
    {
        $http({
            method: 'POST',
            url: '/api/CurrencyAPI/GetCurrencyWords/',
            data: $scope.TestInput
        }).then(function successCallback(response) {               
            $scope.TestResult = response.data;
            $scope.Clear();
        }, function errorCallback(response) {           
            alert("Error : " + response.data.ExceptionMessage);
        });
    }

    $scope.TestInput = {
        Name: '',
        Number: ''      
    };
    $scope.TestResult = {
        Name: '',
        CurrencyWords: ''
    };

    $scope.Clear = function () {
        $scope.TestInput.Name = '';
        $scope.TestInput.Number = '';      
    }
   

});

