﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AKQA.Web.Models
{
    public class TestOutput
    {
        public string Name { get; set; }
        public string CurrencyWords { get; set; }
    }
}