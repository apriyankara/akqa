﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AKQA.Library
{
    public class CurrencyHandler : ICurrencyHandler
    {
        /// <summary>
        /// method converts the given number input into currency words
        /// </summary>
        /// <param name="strNumber">number input</param>
        /// <returns></returns>
        public string ConvertNumberToCurrencyWords(string strNumber)
        {
            string strResult = string.Empty;
            string strNegativeFlag = string.Empty;

            try
            {
                strNumber = Convert.ToDouble(strNumber).ToString();

                if (strNumber.Contains("-"))
                {
                    strNegativeFlag = "Minus ";
                    strNumber = strNumber.Substring(1, strNumber.Length - 1);
                }
                if (strNumber == "0")
                {
                    strResult = "Zero";
                }
                else
                {
                    strResult = strNegativeFlag + Utility.GetCurrencyWords(strNumber);
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult.ToUpper();
        }
    }
}
