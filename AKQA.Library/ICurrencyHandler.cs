﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AKQA.Library
{
    public interface ICurrencyHandler
    {
        string ConvertNumberToCurrencyWords(string number);
    }
}
